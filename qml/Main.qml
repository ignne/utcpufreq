/*
 * Copyright (C) 2024  Malte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcpufreq is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'utcpufreq.ignne'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent
        id: mainPage

        header: PageHeader {
            id: header
            title: i18n.tr('CPU Frequency Monitor')
        }


        Label {
            id: cpuInfoTxt
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: header.bottom
            anchors.margins: {
                top: units.gu(1)
                bottom: units.gu(1)
            }
        }

        RowLayout {
            anchors.right: parent.right
            id: stressThreads
            anchors.left: parent.left
            anchors.margins: {
                top: units.gu(3)
            }
            Label {
                id: stressThreadsTxt
                text: i18n.tr('Threads:')
            }
            TextField {
                id: stressThreadsInp
                validator: IntValidator{bottom: 1; top: 99;}
                text: "1"
                inputMethodHints: Qt.ImhDigitsOnly
                Layout.fillWidth: true
                // if focus lost and value is bad, set default value
                onActiveFocusChanged: {
                        if (!activeFocus) {
                            if (text == "" || text == "0") {
                                text = "1";
                            }
                        }
                    }
            }
        }
        Button {
            id: stressBtn
            property bool isActive: false
            text: isActive? i18n.tr('Stop Stress Test!') : i18n.tr('Start Stress Test')
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: stressThreads.bottom
            anchors.margins: {
                top: units.gu(1)
            }
            color: isActive? LomiriColors.orange : LomiriColors.lightGrey
            onClicked: if(isActive) {
                            python.call('main.stop_stress_test', [], () => {isActive = false;});
                       } else {
                            python.call('main.start_stress_test', [stressThreadsInp.text], () => {isActive = true;});
                       }
        }

    }

    Timer {
        id: updateTimer
        interval: 1000
        running: false
        repeat: true
        onTriggered: {
            python.updateGraphs()
        }
    }


    Python {
        id: python

        property var cpuNumToGraphMap: new Map()


        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));
            let initDone = false;
            importModule('main', function() {
                python.call('main.init', [], function(cpu_nums) {
                    initDone = true;
                    python.createGraphs(cpu_nums);
                });
            });
        }

        function createGraphs(cpu_nums) {
            console.log("got cpus: " + cpu_nums);
            let prevItem = cpuInfoTxt;
            cpu_nums.sort();
            for (const cpu_num of cpu_nums) {
                let component = Qt.createComponent("CpuGraph.qml");
                while(component.status !== Component.Ready) {
                    console.log(component.errorString());
                }
                let graph = component.createObject(mainPage);
                graph.objectName = "graph" + cpu_num;
                graph.anchors.left = mainPage.left;
                graph.anchors.right = mainPage.right;
                graph.anchors.top = prevItem.bottom;
                prevItem = graph;
                python.cpuNumToGraphMap.set(cpu_num, graph);
            }
            stressThreads.anchors.top = prevItem.bottom;
            updateGraphs();
            python.call('main.get_cpu_desc', [], function(desc) {
                cpuInfoTxt.text = desc;
            });
            updateTimer.running = true;
        }

        function updateGraphs() {
            python.call('main.get_cur_freqs', [], function(freqArr) {
                for (const num_and_freq of freqArr) {
                    ///console.log("got freqs for " + num_and_freq[0] + ": " + num_and_freq[1] + " [" + num_and_freq[2] + ", " + num_and_freq[3] + "]");
                    let graph = python.cpuNumToGraphMap.get("" + num_and_freq[0]);

                    // assume the cpu is offline or deactivated if a value is 0
                    // TODO test this
                    //graph.isDeactivated = (num_and_freq[1] == 0 || num_and_freq[2] == 0 || num_and_freq[3] == 0);

                    if (num_and_freq[3] < num_and_freq[2]) {
                        // max < min is weird
                        num_and_freq[3] = num_and_freq[2];
                    }
                    if (num_and_freq[1] < num_and_freq[2]) {
                        // curr < min is also weird
                        num_and_freq[2] = num_and_freq[1];
                    }
                    if (num_and_freq[3] < num_and_freq[1]) {
                        // finally, max < curr is weird
                        num_and_freq[3] = num_and_freq[1];
                    }

                    graph.freqVal = num_and_freq[1];
                    graph.minFreq = num_and_freq[2];
                    graph.maxFreq = num_and_freq[3];
                }
            });
        }



        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
