import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    property int freqVal: 1000
    property int minFreq: 500
    property int maxFreq: 1500
    property bool isDeactivated: true
    width: units.gu(30)
    height: units.gu(2)
    anchors.margins: { top: units.gu(1) }
    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        color: isDeactivated ? LomiriColors.darkGrey : LomiriColors.lightGrey
        width: 50
        height: parent.height
    }
    Rectangle {
        anchors.left: parent.left
        width: parent.width * (freqVal-minFreq)/(maxFreq-minFreq) + 5
        color: LomiriColors.green
        height: parent.height
    }
    Label {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.botton
        text: minFreq
        color: "white"
    }
    Label {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.botton
        text: maxFreq
        color: "white"
    }
    Label {
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.botton
        horizontalAlignment: Text.AlignHCenter
        text: freqVal
        color: "white"
    }
}



