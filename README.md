# CPU Frequency Monitor for Ubuntu Touch

Displays the following information for each core:

- Minimum scaling freq
- Maximum scaling freq
- Current scaling freq
- CPU name

Additionally, you can start a (very simple) stress test, to see how the CPU scaling behaves under load.

## Installing

[![OpenStore](https://open-store.io/badges/en_US.svg){width=30%}](https://open-store.io/app/utcpufreq.ignne)

## Screenshots

![intel](screenshots/intel.png){width=48%}
![ubuntu-touch](screenshots/oneplus6.png){width=48%}

## License

Copyright (C) 2024  Malte

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
