'''
 Copyright (C) 2024  Malte

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 utcpufreq is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import glob
import subprocess


IS_BIG = 1
IS_LITTLE = 2
IS_UNKNOWN = 0

cpu_infos = []

stress_process = None


class CpuInfo:
    """Holds and queries info about one CPU core"""
    def __init__(self, num, path, big_little=0):
        self.num = num
        self.path = path
        self.big_little = big_little

    def get_cpu_num(self):
        return self.num

    def get_cur_freq(self):
        freq = 0
        try:
            with open(self.path + "/cpufreq/scaling_cur_freq") as f:
                freq = f.read()
        except OSError:
            pass
        return int(freq) / 1000

    def get_min_freq(self):
        freq = 0
        try:
            with open(self.path + "/cpufreq/scaling_min_freq") as f:
                freq = f.read()
        except OSError:
            pass
        return int(freq) / 1000

    def get_max_freq(self):
        freq = 0
        try:
            with open(self.path + "/cpufreq/scaling_max_freq") as f:
                freq = f.read()
        except OSError:
            pass
        return int(freq) / 1000


def init():
    global cpu_infos
    base_path = "/sys/devices/system/cpu/"
    cpu_dirs = glob.glob(base_path + "cpu[0-9]*")
    cpu_nums = []
    for cpu_dir in cpu_dirs:
        try:
            num = cpu_dir[cpu_dir.rindex("u")+1:]
            cpu_info = CpuInfo(num, cpu_dir)
            cpu_nums.append(num)
            cpu_infos.append(cpu_info)
            # TODO: big_little
        except OSError:
            continue
    return cpu_nums


def get_cur_freqs():
    retval = []
    for cpu_info in cpu_infos:
        retval.append([cpu_info.get_cpu_num(), cpu_info.get_cur_freq(),
                       cpu_info.get_min_freq(), cpu_info.get_max_freq()])
    return retval


def get_cpu_desc():
    desc = "Unknown CPU"
    try:
        with open("/proc/cpuinfo") as f:
            desc = f.read()
            startIdx = desc.find("model name")
            if startIdx == -1:
                startIdx = desc.find("Hardware")
            if startIdx == -1:
                startIdx = desc.find("Processor")
            if startIdx == -1:
                raise ValueError()
            desc = desc[desc.find(":", startIdx)+1:desc.find("\n", startIdx)]
    except OSError:
        pass
    except ValueError:
        pass
    return desc


def start_stress_test(num_threads):
    global stress_process
    stress_process = subprocess.Popen(["./stress-test", str(num_threads)])


def stop_stress_test():
    global stress_process
    stress_process.terminate()
