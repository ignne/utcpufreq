/* Simple stress test that runs <n> threads */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *endless_loop() {
    int i = 0;
    for(;;) {
    }
}

int main(int argc, char *argv[]) {
    if(argc != 2) {
        printf("Usage: stress-test <num_threads>\n");
        return 1;
    }
    int num_threads = 1;
    sscanf(argv[1], "%d", &num_threads);
    pthread_t t; 
    for(int i = 0; i < num_threads; i++) {
        pthread_create(&t, NULL, endless_loop, NULL);
    }
    pthread_join(t, NULL);
    return 0;
}

